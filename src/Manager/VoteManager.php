<?php

namespace App\Manager;

use App\Entity\Movie;
use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;

class VoteManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ThirdPartyManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Users $user
     * @param Movie $movie
     *
     * @return bool
     */
    public function voteMovie(Users $user, Movie $movie)
    {
        // Only add the movie if the user has not voted 3 times yet
        if ($user->getVotes()->count() > 2) {
            return false;
        } else {
            $user->addVote($movie);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return true;
    }

    /**
     * @return mixed
     */
    public function findAllVotingUsers()
    {
        return $this->entityManager
            ->getRepository(Users::class)
            ->findAllVotingUsers();
    }
}
