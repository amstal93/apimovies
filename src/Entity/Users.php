<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 * @ORM\Table(
 *     name="users",
 *     uniqueConstraints={@UniqueConstraint(name="email_unique",columns={"email"})}
 * )
 *
 */
class Users
{
    /**
     * Users constructor.
     */
    public function __construct()
    {
        $this->creationDate = new \DateTime();
        $this->votes        = new ArrayCollection();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nickname;

    /**
     * @ORM\Column(type="string", length=255)
     *
     */
    private $email;

    /**
     * @ORM\Column(type="datetime")
     */
    private $birthDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    /**
     * Many User have Many Votes.
     *
     * @ORM\ManyToMany(targetEntity="Movie", fetch="EAGER")
     * @ORM\JoinTable(name="votes",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="movie_id", referencedColumnName="id", unique=false)}
     *      )
     */
    private $votes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @return Collection|Movie[]
     */
    public function getVotes(): Collection
    {
        return $this->votes;
    }

    /**
     * @param $votes
     *
     * @return Users
     */
    public function setVotes($votes): self
    {
        $this->votes = $votes;

        return $this;
    }

    /**
     * @param Movie $movie
     *
     * @return Users
     */
    public function addVote(Movie $movie): self
    {
        if (!$this->votes->contains($movie)) {
            $this->votes[] = $movie;
        }

        return $this;
    }

    /**
     * @param Movie $movie
     *
     * @return Users
     */
    public function removeVote(Movie $movie): self
    {
        if ($this->votes->contains($movie)) {
            $this->votes->removeElement($movie);
        }

        return $this;
    }
}
