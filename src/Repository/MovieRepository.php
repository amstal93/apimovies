<?php

namespace App\Repository;

use App\Entity\Movie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Movie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movie[]    findAll()
 * @method Movie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(RegistryInterface $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Movie::class);
        $this->entityManager = $entityManager;
    }

    /**
     * @return mixed
     */
    public function findBest()
    {
        $sql = "";
        $sql .= "SELECT COUNT(v.movie_id) as vote_count, m.omdb_id as omdbId, m.title as title, m.poster as poster ";
        $sql .= "FROM movie m ";
        $sql .= "INNER JOIN votes v ON m.id = v.movie_id ";
        $sql .= "GROUP BY m.omdb_id, m.title, m.poster ";
        $sql .= "ORDER BY vote_count DESC ";
        $sql .= "LIMIT 1";

        $statement = $this->entityManager->getConnection()->prepare($sql);
        $statement->execute();

        $movie = $statement->fetchAll();

        return empty($movie) ? null : $movie[0];
    }
}
