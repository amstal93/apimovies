# APIMovies
_Prerequisites_ : the list of available movies is fortified in front based on http://www.omdbapi.com/ API. The back register only the arriving selected movies (but there is possible to pre-fill the DB with the list of films for more control on the data)
Once selected, a chosen movie give all infos to our backend API.

## How to use APIMovies
The only tool you need is Docker

At the root of the project, run docker `docker-compose up --build -d`

Go to Symfony container by typing the command `docker-compose run php-fpm bash`

Instantiate **.env** files in `/var/www/.env` and `/var/www/docker/postgres/postgres.env`

Run the following commands :
- `bin/console doctrine:database:create`
- `bin/console doctrine:schema:update --force`

Open a Postman (the best option so far) ans start hacking the routes...
The base url given with docker is `localhost:80`


## Routes
Here are the opened routes of the API. A Front-End Developer can base his developments on this doc.

| Method | Route | Description| Body | Response |
|---|---|---|---|---|
| POST | /new/user  | Create a new user  | JSON  | 201  |
| POST | /vote/movie  | Choose a movie and add a vote for it  | JSON  | 201  |
| DELETE | /unvote/movie  | Create a new user  | JSON  | 201  |
| GET | /see/selected/{userId}  | View the list of the movies of a user | -  | 200  |
| GET | /see/participants  | See all users that have already voted  | -  | 200  |
| GET | /see/results  | See the best movie at a given time  | -  | 200  |
    
    
### Example                                                                      
Body Request - /new/user
```JSON
{
    "nickname" : "John Doe",
    "email" : "johndoe@local.net",
    "birthDate" : "1990-10-10"
}
```

Body Request - /vote/movie
```JSON
{
    "userId" : "1",
    "vote" : {
        "title" : "Wasabi",
        "omdbId" : "tt0281364",
        "poster" : "https://ia.media-imdb.com/images/M/MV5BMTQ4NTE4OTA5N15BMl5BanBnXkFtZTcwODc4MzU2MQ@@._V1_SX300.jpg"
    }
}
```

Body Request - /unvote/movie 
```JSON
{
  "userId" : "1",
  "vote" : {
    "omdbId" : "tt0281364"
  }
}
```

Body Response - /see/selected/{userId}
```JSON
{
  "choices": [
    {
      "title": "Wasabi",
      "omdbId": "tt0281364",
      "poster": "https://ia.media-imdb.com/images/M/MV5BMTQ4NTE4OTA5N15BMl5BanBnXkFtZTcwODc4MzU2MQ@@._V1_SX300.jpg"
    }
  ],
  "code": 0
}
```

Body Response - /see/participants
```JSON
{
  "participants": 
    [
      {
        "nickname":"John Bon",
        "id":1
      },
      {
        "nickname":"John Con",
        "id":2
      }
    ],
  "code":0
}
```

Body Response - /see/results
```JSON
{
  "first": {
    "vote_count": 2,
    "omdbid": "tt0281364",
    "title": "Wasabi",
    "poster": "https://ia.media-imdb.com/images/M/MV5BMTQ4NTE4OTA5N15BMl5BanBnXkFtZTcwODc4MzU2MQ@@._V1_SX300.jpg"
  },
  "code": 0
}
 ```
                                                                                                                 
### Code retour
- 0 : success
- 1 : JSON body does not match format
- 2 : there are missing keys into the JSON
- 3 : no user found for the given id
- 4 : the user has already voted 3 times 
- 5 : no movie found for the given ormdbId

## Todo
The next sprints will include, a stronger **constraints validation** and a **param converter** for the relevant routes.

By Pec